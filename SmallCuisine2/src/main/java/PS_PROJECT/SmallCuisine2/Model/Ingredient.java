package PS_PROJECT.SmallCuisine2.Model;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Ingredient {

        @Id
        private Integer id;
        private String type;
        private String name;

        public Ingredient(){}

        public Ingredient(Integer id, String type, String name) {
            this.id = id;
            this.type = type;
            this.name = name;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }


