package PS_PROJECT.SmallCuisine2.Repository;

import PS_PROJECT.SmallCuisine2.Model.Person;
import org.springframework.data.repository.CrudRepository;

public interface PersonRepository extends CrudRepository<Person,Integer> {
}
