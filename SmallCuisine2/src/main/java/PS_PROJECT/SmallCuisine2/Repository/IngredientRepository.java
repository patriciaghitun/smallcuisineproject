package PS_PROJECT.SmallCuisine2.Repository;

import PS_PROJECT.SmallCuisine2.Model.Ingredient;
import org.springframework.data.repository.CrudRepository;

public interface IngredientRepository extends CrudRepository<Ingredient,Integer>{
}
