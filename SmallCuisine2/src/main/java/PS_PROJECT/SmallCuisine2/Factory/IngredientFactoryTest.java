package PS_PROJECT.SmallCuisine2.Factory;

public class IngredientFactoryTest {
    public static void main(String[] args) {

        IngredientFactory ingredientFactory = new IngredientFactory();

        //INGREDIENT 1 - ii dau sa apelez ce am in DAIRY
        IngredientInterface shape1 = ingredientFactory.getIngredientType("DAIRY");
        System.out.println(shape1.showIngredient());

        //INGREDIENT 2
        IngredientInterface shape2 = ingredientFactory.getIngredientType("VEGETABLES");
        System.out.println(shape2.showIngredient());



    }
}
