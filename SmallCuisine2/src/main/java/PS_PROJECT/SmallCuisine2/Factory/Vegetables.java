package PS_PROJECT.SmallCuisine2.Factory;

public class Vegetables implements IngredientInterface {


    private String name;
    private int nrFats;

    public Vegetables(){}

    public Vegetables(String name, int nrFats) {
        this.name = name;
        this.nrFats = nrFats;
    }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNrFats() {
        return nrFats;
    }

    public void setNrFats(int nrFats) {
        this.nrFats = nrFats;
    }

    @Override
    public String showIngredient() {
        return "This is VEGETABLE";
    }
}
