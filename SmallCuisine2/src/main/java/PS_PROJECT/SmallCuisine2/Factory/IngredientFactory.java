package PS_PROJECT.SmallCuisine2.Factory;

public class IngredientFactory {

    //use getShape method to get object of type shape
    public IngredientInterface getIngredientType(String type){

        if(type.equalsIgnoreCase("DAIRY")){
            return new Dairy();

        } else if(type.equalsIgnoreCase("VEGETABLES")){
            return new Vegetables();
        }
         return null;
    }
}
