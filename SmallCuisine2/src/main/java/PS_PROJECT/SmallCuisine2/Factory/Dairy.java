package PS_PROJECT.SmallCuisine2.Factory;

public class Dairy implements IngredientInterface {

    private String name;
    private int nrProteins;

    public Dairy(){}

    public Dairy(String name, int nr) {
        this.name = name;
        this.nrProteins=nr;
    }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNrProteins() {
        return nrProteins;
    }

    public void setNrProteins(int nrProteins) {
        this.nrProteins = nrProteins;
    }

    @Override
    public String showIngredient() {
        return "This is DAIRY";
    }
}
