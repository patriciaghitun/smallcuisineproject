package PS_PROJECT.SmallCuisine2.Controller;

import PS_PROJECT.SmallCuisine2.Model.Ingredient;
import PS_PROJECT.SmallCuisine2.Repository.IngredientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path="/ingredient")

public class IngredientController {
    @Autowired
    private IngredientRepository ingredientRepository;


    /**
     * Adds a new ingredient to the database given as an ingredient object in the RequestBody
     * @param newIngredient
     * @return The saved ingredient
     */
    @PostMapping(path="/add")
    public @ResponseBody Ingredient addNewIngredient (@RequestBody Ingredient newIngredient) {
        return ingredientRepository.save(newIngredient);
    }

    /**
     * Deletes an ingredient given as an object
     * @param id (Integer sent through PathVariable)
     * @return String with the response.
     */
    @DeleteMapping(path="/delStandard/{id}")
    public String deleteIngredient (@PathVariable Integer id)
    {
        String idString=id.toString();
        if(findIngredientById(idString).equals("Ingredient found"))
        {
            ingredientRepository.deleteById(Integer.parseInt(idString));
            return "Ingredient deleted";
        }else return "Ingredient not found in the database";

    }

    /**
     * Deletes an ingredient by its Id(string in the RequestBody)
     * @param idIngredient
     * @return A response for the user to know if the ingredient was deleted or it didn't exist in the first place in our database.
     */
    @DeleteMapping(path="/delById")
    public String deleteIngredientById(@RequestBody String idIngredient){
        int id=Integer.parseInt(idIngredient);
        if(findIngredientById(idIngredient).equals("Ingredient found"))
        {
            ingredientRepository.deleteById(id);
            return "Ingredient deleted";
        }else return "Ingredient not found in the database";

    }

    /**
     * Find a specific ingredinet by an id (string) in the body of the request
     * @param idIngredient
     * @return A string response in order to know if the specified ingredient is present or not in the database.
     */
    @PostMapping(path="/findById")
    public String findIngredientById(@RequestBody String idIngredient){
        int id=Integer.parseInt(idIngredient);
        if(ingredientRepository.findById(id).isPresent()==true)
        {
            return "Ingredient found";
        }else return "Ingredient not found";
    }


    /**
     * Finds all the ingredients present in the database
     * @return List of Iterable Ingredient objects that are present in the database at the time.
     */
    @GetMapping(path="/all")
    public @ResponseBody Iterable<Ingredient> getAllIngredients() {
        return ingredientRepository.findAll();
    }


}
