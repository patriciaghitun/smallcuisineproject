package PS_PROJECT.SmallCuisine2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SmallCuisine2Application {

	public static void main(String[] args) {
		SpringApplication.run(SmallCuisine2Application.class, args);
	}

}
