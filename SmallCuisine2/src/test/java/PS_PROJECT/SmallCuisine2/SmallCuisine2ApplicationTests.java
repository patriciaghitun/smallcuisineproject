package PS_PROJECT.SmallCuisine2;

import PS_PROJECT.SmallCuisine2.Factory.IngredientFactory;
import PS_PROJECT.SmallCuisine2.Factory.IngredientInterface;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;


@RunWith(SpringRunner.class)
@SpringBootTest
public class SmallCuisine2ApplicationTests {

	@Mock
	IngredientFactory ingredientFactory ;
	IngredientInterface type1;
	IngredientInterface type2;

	@Before
	public void init() {
		ingredientFactory= new IngredientFactory();
	}


	@Test
	public void testShowIngredient(){

		//Mockito.when(ingredientFactory.getIngredientType("DAIRY")).thenReturn(new  );
		//String ingredientFound = ingredientDAO.findIngredientById(1);

		//verificare pentru DAIRY
		type1 = ingredientFactory.getIngredientType("DAIRY");
		System.out.println("print : "+type1.showIngredient());
		assertEquals("This is DAIRY",type1.showIngredient());

		//verificare pentru VEGETABLES
		type2 = ingredientFactory.getIngredientType("VEGETABLES");
		System.out.println("print : "+type2.showIngredient());
		assertEquals("This is VEGETABLE",type2.showIngredient());
	}

}
